### Aplicación

Este servicio permite validar que una o varias geometrías (en GeoJSON) se ubican integramente en el límite autonómico y opcionalmente en la provincia, comarca, municipio o parcela indicados.

### Uso

Para validar un archivo GeoJSON se invocará /ValidadorGeomWS/ValidadorGeomWS mediante una petición POST en formato multipart/form-data con los siguientes parámetros:

- file: fichero GeoJSON
- layer (opcional): tipo de límite a comprobar adicionalmente al autonómico. Las opciones soportadas son: Municipio, Comarca, Provincia, ParRus, BusParUrb o ParSIGPAC
- code (opcional): código de la delimitación (en la capa indicada en el parámetro layer) a comprobar

Como respuesta, en caso de resultar válido el GeoJSON se devolverá un JSON con el identificador único (uuid) asignado a las geometrías del fichero, que se almacenarán en la tabla PostGIS geometrias_validas. En caso de no resultar válido el servicio devuelve un JSON con el error detectado.

Se incluye un cliente simple de prueba del servicio en /ValidadorGeomWS.

### Dependencias

Para el funcionamiento del servicio ValidadorGeomWS es necesario disponer en Postgres de las tablas de almacenamiento de las geometrías tanto válidas como inválidas. Dichas tablas se pueden crear con los comandos SQL incluidos en el fichero doc/tablas_bbdd.sql. 
Asimismo requiere la existencia de las tablas con el límite autonómico, límites provinciales, límites comarcales, límites municipales, delimitación de parcelas rústicas y urbanas y delimitación de parcelas SIGPAC. Todas estas tablas deben contener un atributo denominado shape con las geometrías en EPSG:25830 y un campo de identificación de cada geometría. El nombre de las tablas y del campo de identificación se puede configurar en src/main/webapp/WEB-INF/config/validador.properties, salvo en el caso del límite comarcal que debe denominarse geopub.t101d_limiteautonomico y no requiere campo de identificación.

### Instalación
Para instalarlo, generar el war y desplegar en el servidor de aplicaciones
