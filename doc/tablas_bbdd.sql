
CREATE TABLE geometrias_validas
(
  pk serial NOT NULL,
  uuid character varying,
  layer character varying,
  code character varying,
  shape geometry,
  upload_date timestamp without time zone,
  CONSTRAINT pk_geometrias_validas PRIMARY KEY (pk)
  
);


CREATE TABLE geometrias_invalidas
(
  pk serial NOT NULL,
  error character varying,
  layer character varying,
  code character varying,
  shape geometry,
  upload_date timestamp without time zone,
  CONSTRAINT pk_geometrias_invalidas PRIMARY KEY (pk)
 
);
