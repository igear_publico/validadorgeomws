﻿package es.ideariumConsultores.validation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;


//import javax.xml.parsers.*;

public class Servlet extends HttpServlet {

	public  Logger logger = Logger.getLogger("ValidadorGeomsWS");

	public void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);
		GeoJSONValidator.initProperties(/*servletConfig.getInitParameter("properties_path")*/
				getServletContext().getRealPath("/WEB-INF/config")+"/validador.properties");


	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{logger.debug("GET");
	try{
		request.setCharacterEncoding("UTF-8");
	}
	catch(Exception ex){
		logger.error("No se puede establecer la petición como UTF-8", ex);
	}
	String op = request.getParameter("REQUEST");
	if ((op!=null)&&(op.equalsIgnoreCase("LOG"))){
		try{
			Properties logProp = new Properties();

			logProp.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes")+"/log4j.properties"));
			FileInputStream log = new FileInputStream(logProp.getProperty("log4j.appender.A1.File"));

			ServletOutputStream sos = response.getOutputStream();
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("application/file");
			response.setHeader("Content-Disposition", "attachment; filename=\"ValidadorGeomsWS.log\"");
			byte[] logBytes = new byte[512];
			while (log.read(logBytes)>0){
				sos.write(logBytes);
				logBytes = new byte[512];
			}
			sos.flush();
		}
		catch(Exception ex){
			try{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
			}catch(IOException ex2){
				logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
				return;
			}
			logger.error("Error en la exportación del log",ex);
		}
	}




	}
	/**
	 * peticion por POST
	 * @param request peticion que representa una tabla que quiere convertise en mapa 
	 * @param response resultado tipo mapa 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{

		String contentType = request.getContentType();
		logger.debug("POST "+contentType);


		String layer=null;
		String code=null;
		FileItem file=null;


		int maxMemSize = 5000 * 1024;


		// Verify the content type


		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		try{

			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();


			while ( i.hasNext () ) 
			{
				FileItem fi = (FileItem)i.next();
				if ( fi.isFormField () )	
				{
					String param = fi.getFieldName();
					if (param.equalsIgnoreCase("layer")){
						layer = fi.getString();
					}
					else if (param.equalsIgnoreCase("code")){
						code = fi.getString();
					}
				}

				else{

					file=fi;



				}
			}

			try{
				logger.debug("layer "+layer);
				logger.debug("code "+code);
				GeoJSONValidator validator;
				try{
					validator = new GeoJSONValidator(file, layer);
				}
				catch(Exception ex){
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, ex.getMessage());
					return;
				}
				String resultado;
				try{
					resultado = validator.validate(code);
				}
				catch(Exception ex){
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
					return;
				}

				response.setStatus(HttpServletResponse.SC_OK);

				ServletOutputStream sos = response.getOutputStream();

				response.setContentType("application/json");

				sos.write(resultado.getBytes());
				sos.flush();


			}
			catch(IOException ex){

				logger.error("No se pudo enviar respuesta de error (error interno)",ex);
				return;

			}

		}
		catch(Exception ex){

			logger.error("Error procesando su solicitud",ex);
			return;

		}

	}
}