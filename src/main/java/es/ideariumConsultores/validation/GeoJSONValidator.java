package es.ideariumConsultores.validation;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import conexion.conexionBD;

public class GeoJSONValidator {

	JSONArray features;
	static Properties properties;
	public static  Logger logger = Logger.getLogger("ValidadorGeomsWS");
	Connection conexion;
	String layer;
	static public void initProperties(String propertiesPath){

		properties = new Properties();
		try{
			properties.load(new FileInputStream(propertiesPath));
		}
		catch(Exception ex){
			logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
			//	ex.printStackTrace();	
		}

	}


	public  GeoJSONValidator(FileItem geoJSON, String layer ) throws Exception{
		if ((layer != null) && (layer.length()>0) && (properties.getProperty("tabla_"+layer)==null)){
			throw new Exception("Capa "+layer+" no soportada");
		}
		JSONObject json = new JSONObject(geoJSON.getString());
		features = json.getJSONArray("features");
		if (features.length()==0){
			throw new Exception("El archivo no contiene ninguna feature");
		}
		if((layer != null) && (layer.length()>0)){
			this.layer = layer;	
		}

	}


	public String validate(String code) throws Exception{
		String uuid;

		conexion = conexionBD.getConnection("conexVG");

		try{
			for (int i=0;i<features.length(); i++){
				JSONObject feature = features.getJSONObject(i);

				if(!validateGeom(feature.getJSONObject("geometry").toString(),  code)){
					String error =(layer==null ? "La geometría está completa o parcialmente fuera de Aragón" :"La geometría está completa o parcialmente fuera de la feature indicada ("+layer+" "+code+")");
					Statement stmt = conexion.createStatement();
					String geometry = "st_transform(st_setsrid(ST_GeomFromGeoJSON('"+feature.getJSONObject("geometry").toString()+"'),4326),25830)";
					logger.debug("INSERT INTO public.geometrias_invalidas(error,shape,layer,code,upload_date) VALUES ('"+error+"',"+geometry+","+(layer != null ?" '"+layer+"','"+code+"'":"null,null")+",current_timestamp)");
					stmt.executeUpdate("INSERT INTO public.geometrias_invalidas(error,shape,layer,code,upload_date) VALUES ('"+error+"',"+geometry+","+(layer != null ?" '"+layer+"','"+code+"'":"null,null")+",current_timestamp)");
					return "{\"error\":\""+error+"\"}";
				}
			}
			uuid = UUID.randomUUID().toString();

			for (int i=0;i<features.length(); i++){
				JSONObject feature = features.getJSONObject(i);
				Statement stmt = conexion.createStatement();
				String geometry = "st_transform(st_setsrid(ST_GeomFromGeoJSON('"+feature.getJSONObject("geometry").toString()+"'),4326),25830)";
				logger.debug("INSERT INTO public.geometrias_validas(uuid,shape,layer,code,upload_date) VALUES ('"+uuid+"',"+geometry+","+(layer != null ?" '"+layer+"','"+code+"'":"null,null")+",current_timestamp)");
				stmt.executeUpdate("INSERT INTO public.geometrias_validas(uuid,shape,layer,code,upload_date) VALUES ('"+uuid+"',"+geometry+","+(layer != null ?" '"+layer+"','"+code+"'":"null,null")+",current_timestamp)");

			}
		}
		catch(Exception ex){
			logger.error("Error validando geoJSON", ex);
			throw ex;
		}
		finally{
			conexion.close();
		}

		return "{\"uuid\":\""+uuid+"\"}";

	}

	public boolean validateGeom(String geom, String code) throws Exception{
		Statement stmt = conexion.createStatement();
		String geometry = "st_transform(st_setsrid(ST_GeomFromGeoJSON('"+geom+"'),4326),25830)";
		String query;
		if (layer == null){
			query ="SELECT 1 as valido FROM geopub.t101d_limiteautonomico WHERE (ST_CONTAINS(shape,"+geometry+") or st_equals(shape,"+geometry+"))"; 
		}
		else{
			query ="SELECT 1 as valido FROM "+properties.getProperty("tabla_"+layer)+" WHERE "+properties.getProperty("campo_"+layer)+"='"+code+"' AND (ST_CONTAINS(shape,"+geometry+") or st_equals(shape,"+geometry+"))";
		}
		logger.debug(query);
		ResultSet rs = stmt.executeQuery(query);
		boolean valido=rs.next();
		rs.close();
		stmt.close();
		return valido;
	}
}
