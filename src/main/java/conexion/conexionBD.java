package conexion;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class conexionBD {

    public static Connection getConnection(String nombreConexion) throws Exception
    {
          return getPooledConnection(nombreConexion);
    }

    public static Connection getPooledConnection(String nombreConexion) throws Exception{
       Connection conn = null;

        try{
          Context ctx = new InitialContext();
          if(ctx == null )
              throw new Exception("No Context");

          DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/"+nombreConexion);

          if (ds != null) {
             conn = ds.getConnection();
            return conn;
          }else{
              return null;
          }

        }catch(Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}


